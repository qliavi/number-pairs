Number Pairs
============

A game where your remember where two cards with the same number were.

The app is available at http://number-pairs.qliavi.com/.

For technical support create an issue or contact us at info@qliavi.com.
