function Time () {

    function beginInterval (tick) {
        interval = setInterval(tick, 100)
    }

    function largeTick () {
        value++
    }

    function smallTick () {
        value++
        if (value > maxValue) {
            clearInterval(interval)
            node.nodeValue = '>' + FormatTime(maxValue)
            interval = beginInterval(largeTick)
        } else {
            node.nodeValue = FormatTime(value)
        }
    }

    var value = 0,
        maxValue = 10000

    var node = TextNode('0')

    var element = Div('Time')
    element.appendChild(node)
    element.style.backgroundImage = 'url(images/time.svg)'

    var interval

    return {
        element: element,
        getValue: function () {
            return value
        },
        pause: function () {
            clearInterval(interval)
        },
        resume: function () {
            if (value > maxValue) beginInterval(largeTick)
            else beginInterval(smallTick)
        },
    }

}
