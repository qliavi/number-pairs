function MainPanel () {

    function showMenu () {
        element.appendChild(menu.element)
        setTimeout(function () {
            menu.show(menu.enable)
        }, 50)
    }

    function newBoard (difficulty) {

        function hideBoard (callback) {
            board.hide(function () {
                element.removeChild(board.element)
                callback()
            })
        }

        var size = sizes[difficulty],
            horizontal = size.horizontal,
            vertical = size.vertical

        var board = Board(difficulty, horizontal, vertical, function () {

            function hidePausePanel (callback) {
                pausePanel.disable()
                pausePanel.hide(function () {
                    element.removeChild(pausePanel.element)
                    callback()
                })
            }

            var pausePanel = PausePanel(function () {
                board.enable()
                hidePausePanel(function () {})
            }, function () {
                hidePausePanel(function () {})
                hideBoard(function () {
                    newBoard(difficulty)
                })
            }, function () {
                hidePausePanel(function () {})
                hideBoard(function () {})
                showMenu()
            })
            element.appendChild(pausePanel.element)
            board.disable()
            setTimeout(function () {
                pausePanel.show(pausePanel.enable)
            }, 50)

        }, function (time, steps) {

            var timeDivisor = Math.max(1, time - horizontal * vertical),
                stepDivisor = Math.max(1, steps - horizontal * vertical),
                score = Math.round(1000000 / timeDivisor / stepDivisor),
                highScore = GetHighScore(difficulty)

            var recordSet = score > highScore
            if (recordSet) {
                localStorage['highScore_' + difficulty] = JSON.stringify(score)
            }

            hideBoard(function () {})
            board.disable()
            var donePanel = DonePanel(time, steps, score, highScore, function () {
                donePanel.disable()
                donePanel.hide(function () {})
                if (recordSet) menu.updateHighScore()
                showMenu()
            })
            element.appendChild(donePanel.element)
            setTimeout(function () {
                donePanel.show(donePanel.enable)
            }, 50)

        })
        element.appendChild(board.element)
        setTimeout(function () {
            board.show(board.enable)
        }, 50)

    }

    var sizes = {
        easy: {
            horizontal: 4,
            vertical: 5,
        },
        medium: {
            horizontal: 5,
            vertical: 6,
        },
        hard: {
            horizontal: 6,
            vertical: 7,
        },
    }

    var menu = Menu(function (difficulty) {
        menu.disable()
        menu.hide(function () {
            element.removeChild(menu.element)
        })
        newBoard(difficulty)
    })

    var element = Div('MainPanel')

    showMenu()

    return { element: element }

}
