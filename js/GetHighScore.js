function GetHighScore (difficulty) {
    var score
    try {
        score = JSON.parse(localStorage['highScore_' + difficulty])
        if (!isFinite(score)) score = 0
    } catch (e) {
        score = 0
    }
    return score
}
