function DonePanel (time, steps, score, highScore, okListener) {

    var classPrefix = 'DonePanel'

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode('WELL DONE!'))

    var timeElement = Div(classPrefix + '-time')
    timeElement.appendChild(TextNode('TIME: ' + FormatTime(time)))

    var stepsElement = Div(classPrefix + '-steps')
    stepsElement.appendChild(TextNode('STEPS: ' + steps))

    var yourScoreElement = Div(classPrefix + '-yourScore')
    yourScoreElement.appendChild(TextNode('YOUR SCORE: ' + score))

    var okElement = Div(classPrefix + '-ok Button')
    okElement.appendChild(TextNode('OK'))

    var okClick = OnClick(okElement, okListener)

    var contentElement = Div(classPrefix + '-content Scale')
    contentElement.appendChild(titleElement)
    contentElement.appendChild(timeElement)
    contentElement.appendChild(stepsElement)
    contentElement.appendChild(yourScoreElement)
    if (score > highScore) {
        var newRecordElement = Div(classPrefix + '-newRecord')
        newRecordElement.appendChild(TextNode('NEW RECORD!'))
        contentElement.appendChild(newRecordElement)
    } else {
        var highScoreElement = Div(classPrefix + '-highScore')
        highScoreElement.appendChild(TextNode('HIGH SCORE: ' + highScore))
        contentElement.appendChild(highScoreElement)
    }
    contentElement.appendChild(okElement)

    var contentClassList = contentElement.classList

    var element = Div(classPrefix + ' BackgroundFade')
    element.appendChild(contentElement)

    var classList = element.classList

    return {
        element: element,
        disable: okClick.disable,
        enable: okClick.enable,
        hide: function (callback) {
            classList.remove('visible')
            contentClassList.remove('visible')
            setTimeout(callback, 150)
        },
        show: function (callback) {
            classList.add('visible')
            contentClassList.add('visible')
            setTimeout(callback, 150)
        },
    }

}
