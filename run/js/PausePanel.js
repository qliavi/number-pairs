function PausePanel (resumeListener, restartListener, quitListener) {

    var classPrefix = 'PausePanel'

    var resumeElement = Div(classPrefix + '-resume Button')
    resumeElement.appendChild(TextNode('RESUME'))

    var resumeClick = OnClick(resumeElement, resumeListener)

    var restartElement = Div(classPrefix + '-restart Button')
    restartElement.appendChild(TextNode('RESTART'))

    var restartClick = OnClick(restartElement, restartListener)

    var quitElement = Div(classPrefix + '-quit Button')
    quitElement.appendChild(TextNode('QUIT'))

    var quitClick = OnClick(quitElement, quitListener)

    var contentElement = Div(classPrefix + '-content Scale')
    contentElement.appendChild(resumeElement)
    contentElement.appendChild(restartElement)
    contentElement.appendChild(quitElement)

    var contentClassList = contentElement.classList

    var element = Div(classPrefix + ' BackgroundFade')
    element.appendChild(contentElement)

    var classList = element.classList

    return {
        element: element,
        disable: function () {
            resumeClick.disable()
            restartClick.disable()
            quitClick.disable()
        },
        enable: function () {
            resumeClick.enable()
            restartClick.enable()
            quitClick.enable()
        },
        hide: function (callback) {
            classList.remove('visible')
            contentClassList.remove('visible')
            setTimeout(callback, 150)
        },
        show: function (callback) {
            classList.add('visible')
            contentClassList.add('visible')
            setTimeout(callback, 150)
        },
    }

}
