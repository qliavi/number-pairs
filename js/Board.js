function Board (difficulty, horizontal, vertical, pauseListener, doneListener) {

    function hideCard (card) {
        cards.splice(cards.indexOf(card), 1)
        card.hide(function () {
            numbersElement.removeChild(card.element)
        })
    }

    var classPrefix = 'Board'

    var numbers = (function () {
        var numbers = []
        var length = (horizontal * vertical) / 2
        for (var i = 0; i < length; i++) {
            numbers.push(i)
            numbers.push(i)
        }
        Shuffle(numbers)
        return numbers
    })()

    var cards = []
    var openCards = []

    var numbersElement = Div(classPrefix + '-numbers ' + difficulty)
    for (var y = 0; y < vertical; y++) {
        for (var x = 0; x < horizontal; x++) {
            (function () {
                var number = numbers.pop()
                var card = Card(number, x, y, function () {
                    steps.add()
                    if (card.isOpen()) {
                        card.close()
                        openCards.splice(openCards.indexOf(card), 1)
                    } else {
                        if (openCards.length == 2) {
                            while (openCards.length) openCards.pop().close()
                        }
                        card.open()
                        openCards.push(card)
                        if (openCards.length == 2 &&
                            openCards[0].number == openCards[1].number) {

                            hideCard(openCards[0])
                            hideCard(openCards[1])
                            openCards.splice(0)
                            if (cards.length == 0) {
                                doneListener(time.getValue(), steps.getValue())
                            }

                        }
                    }
                })
                cards.push(card)
                numbersElement.appendChild(card.element)
            })()
        }
    }

    var pauseElement = Div(classPrefix + '-pause Button')
    pauseElement.appendChild(TextNode('PAUSE'))

    var pauseClick = OnClick(pauseElement, pauseListener)

    var time = Time()

    var steps = Steps()

    var element = Div(classPrefix)
    element.appendChild(time.element)
    element.appendChild(steps.element)
    element.appendChild(pauseElement)
    element.appendChild(numbersElement)

    var classList = element.classList

    return {
        element: element,
        disable: function () {
            time.pause()
            pauseClick.disable()
            cards.forEach(function (card) {
                card.disable()
            })
        },
        enable: function () {
            time.resume()
            pauseClick.enable()
            cards.forEach(function (card) {
                card.enable()
            })
        },
        hide: function (callback) {
            classList.remove('visible')
            setTimeout(callback, 150)
        },
        show: function (callback) {
            classList.add('visible')
            setTimeout(callback, 150)
        },
    }

}
