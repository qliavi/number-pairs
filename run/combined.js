(function () {
function Div (className) {
    var div = document.createElement('div')
    div.className = className
    return div
}
;
function Board (difficulty, horizontal, vertical, pauseListener, doneListener) {

    function hideCard (card) {
        cards.splice(cards.indexOf(card), 1)
        card.hide(function () {
            numbersElement.removeChild(card.element)
        })
    }

    var classPrefix = 'Board'

    var numbers = (function () {
        var numbers = []
        var length = (horizontal * vertical) / 2
        for (var i = 0; i < length; i++) {
            numbers.push(i)
            numbers.push(i)
        }
        Shuffle(numbers)
        return numbers
    })()

    var cards = []
    var openCards = []

    var numbersElement = Div(classPrefix + '-numbers ' + difficulty)
    for (var y = 0; y < vertical; y++) {
        for (var x = 0; x < horizontal; x++) {
            (function () {
                var number = numbers.pop()
                var card = Card(number, x, y, function () {
                    steps.add()
                    if (card.isOpen()) {
                        card.close()
                        openCards.splice(openCards.indexOf(card), 1)
                    } else {
                        if (openCards.length == 2) {
                            while (openCards.length) openCards.pop().close()
                        }
                        card.open()
                        openCards.push(card)
                        if (openCards.length == 2 &&
                            openCards[0].number == openCards[1].number) {

                            hideCard(openCards[0])
                            hideCard(openCards[1])
                            openCards.splice(0)
                            if (cards.length == 0) {
                                doneListener(time.getValue(), steps.getValue())
                            }

                        }
                    }
                })
                cards.push(card)
                numbersElement.appendChild(card.element)
            })()
        }
    }

    var pauseElement = Div(classPrefix + '-pause Button')
    pauseElement.appendChild(TextNode('PAUSE'))

    var pauseClick = OnClick(pauseElement, pauseListener)

    var time = Time()

    var steps = Steps()

    var element = Div(classPrefix)
    element.appendChild(time.element)
    element.appendChild(steps.element)
    element.appendChild(pauseElement)
    element.appendChild(numbersElement)

    var classList = element.classList

    return {
        element: element,
        disable: function () {
            time.pause()
            pauseClick.disable()
            cards.forEach(function (card) {
                card.disable()
            })
        },
        enable: function () {
            time.resume()
            pauseClick.enable()
            cards.forEach(function (card) {
                card.enable()
            })
        },
        hide: function (callback) {
            classList.remove('visible')
            setTimeout(callback, 150)
        },
        show: function (callback) {
            classList.add('visible')
            setTimeout(callback, 150)
        },
    }

}
;
function Card (number, x, y, clickListener) {

    var classPrefix = 'Card'

    var alignerElement = Div(classPrefix + '-aligner')

    var numberElement = Div(classPrefix + '-number')
    numberElement.appendChild(TextNode(number))

    var sideElement = Div(classPrefix + '-side')
    sideElement.appendChild(alignerElement)
    sideElement.appendChild(numberElement)

    var contentElement = Div(classPrefix + '-content Button')
    contentElement.appendChild(sideElement)

    var click = OnClick(contentElement, clickListener)

    var element = Div(classPrefix + ' x' + x + ' y' + y)
    element.appendChild(contentElement)

    var contentClassList = contentElement.classList

    var classList = element.classList

    var isOpen = false

    var timeout

    return {
        disable: click.disable,
        element: element,
        enable: click.enable,
        number: number,
        close: function () {
            if (isOpen) {
                isOpen = false
                contentClassList.remove('open')
                classList.add('animating')
                clearTimeout(timeout)
                timeout = setTimeout(function () {
                    contentClassList.remove('halfOpen')
                    contentClassList.remove('sideVisible')
                    timeout = setTimeout(function () {
                        classList.remove('animating')
                    }, 150)
                }, 150)
            }
        },
        hide: function (callback) {
            click.disable()
            setTimeout(function () {
                contentClassList.add('guessed')
                setTimeout(function () {
                    contentClassList.add('hidden')
                    setTimeout(callback, 350)
                }, 150)
            }, 150)
        },
        isOpen: function () {
            return isOpen
        },
        open: function () {
            if (!isOpen) {
                isOpen = true
                contentClassList.add('halfOpen')
                classList.add('animating')
                clearTimeout(timeout)
                timeout = setTimeout(function () {
                    contentClassList.add('open')
                    contentClassList.add('sideVisible')
                    timeout = setTimeout(function () {
                        classList.remove('animating')
                    }, 150)
                }, 150)
            }
        },
    }

}
;
function DifficultyButton (text, className, clickListener) {

    function click () {
        select()
        clickListener()
    }

    function select () {
        classList.add('selected')
    }

    var iconElement = Div('DifficultyButton-icon ' + className)
    iconElement.style.backgroundImage = 'url(images/difficulty.svg)'

    var selectedElement = Div('DifficultyButton-selected')
    selectedElement.style.backgroundImage = 'url(images/selected.svg)'

    var element = Div('DifficultyButton Button ' + className)
    element.appendChild(iconElement)
    element.appendChild(TextNode(text))
    element.appendChild(selectedElement)

    var click = OnClick(element, click)

    var classList = element.classList

    return {
        disable: click.disable,
        element: element,
        enable: click.enable,
        select: select,
        deselect: function () {
            classList.remove('selected')
        },
    }

}
;
function DonePanel (time, steps, score, highScore, okListener) {

    var classPrefix = 'DonePanel'

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode('WELL DONE!'))

    var timeElement = Div(classPrefix + '-time')
    timeElement.appendChild(TextNode('TIME: ' + FormatTime(time)))

    var stepsElement = Div(classPrefix + '-steps')
    stepsElement.appendChild(TextNode('STEPS: ' + steps))

    var yourScoreElement = Div(classPrefix + '-yourScore')
    yourScoreElement.appendChild(TextNode('YOUR SCORE: ' + score))

    var okElement = Div(classPrefix + '-ok Button')
    okElement.appendChild(TextNode('OK'))

    var okClick = OnClick(okElement, okListener)

    var contentElement = Div(classPrefix + '-content Scale')
    contentElement.appendChild(titleElement)
    contentElement.appendChild(timeElement)
    contentElement.appendChild(stepsElement)
    contentElement.appendChild(yourScoreElement)
    if (score > highScore) {
        var newRecordElement = Div(classPrefix + '-newRecord')
        newRecordElement.appendChild(TextNode('NEW RECORD!'))
        contentElement.appendChild(newRecordElement)
    } else {
        var highScoreElement = Div(classPrefix + '-highScore')
        highScoreElement.appendChild(TextNode('HIGH SCORE: ' + highScore))
        contentElement.appendChild(highScoreElement)
    }
    contentElement.appendChild(okElement)

    var contentClassList = contentElement.classList

    var element = Div(classPrefix + ' BackgroundFade')
    element.appendChild(contentElement)

    var classList = element.classList

    return {
        element: element,
        disable: okClick.disable,
        enable: okClick.enable,
        hide: function (callback) {
            classList.remove('visible')
            contentClassList.remove('visible')
            setTimeout(callback, 150)
        },
        show: function (callback) {
            classList.add('visible')
            contentClassList.add('visible')
            setTimeout(callback, 150)
        },
    }

}
;
function FormatTime (value) {
    return Math.floor(value / 10) + '.' + value % 10
}
;
function GetHighScore (difficulty) {
    var score
    try {
        score = JSON.parse(localStorage['highScore_' + difficulty])
        if (!isFinite(score)) score = 0
    } catch (e) {
        score = 0
    }
    return score
}
;
function LoadPanel (loadCallback) {

    function loadNext () {
        var img = new Image
        img.src = 'images/' + images.shift() + '.svg'
        img.onload = function () {
            loaded++
            barElement.style.width = loaded / itemsToLoad * 100 + '%'
            if (loaded == itemsToLoad) loadCallback()
            else loadNext()
        }
    }

    var images = ['difficulty', 'selected', 'steps', 'time']

    var classPrefix = 'LoadPanel'

    var barElement = Div(classPrefix + '-bar')

    var element = Div(classPrefix)
    element.appendChild(barElement)

    var loaded = 0,
        itemsToLoad = images.length

    loadNext()

    return {
        element: element,
        hide: function (callback) {
            element.classList.add('hidden')
            setTimeout(callback, 350)
        },
    }

}
;
function MainPanel () {

    function showMenu () {
        element.appendChild(menu.element)
        setTimeout(function () {
            menu.show(menu.enable)
        }, 50)
    }

    function newBoard (difficulty) {

        function hideBoard (callback) {
            board.hide(function () {
                element.removeChild(board.element)
                callback()
            })
        }

        var size = sizes[difficulty],
            horizontal = size.horizontal,
            vertical = size.vertical

        var board = Board(difficulty, horizontal, vertical, function () {

            function hidePausePanel (callback) {
                pausePanel.disable()
                pausePanel.hide(function () {
                    element.removeChild(pausePanel.element)
                    callback()
                })
            }

            var pausePanel = PausePanel(function () {
                board.enable()
                hidePausePanel(function () {})
            }, function () {
                hidePausePanel(function () {})
                hideBoard(function () {
                    newBoard(difficulty)
                })
            }, function () {
                hidePausePanel(function () {})
                hideBoard(function () {})
                showMenu()
            })
            element.appendChild(pausePanel.element)
            board.disable()
            setTimeout(function () {
                pausePanel.show(pausePanel.enable)
            }, 50)

        }, function (time, steps) {

            var timeDivisor = Math.max(1, time - horizontal * vertical),
                stepDivisor = Math.max(1, steps - horizontal * vertical),
                score = Math.round(1000000 / timeDivisor / stepDivisor),
                highScore = GetHighScore(difficulty)

            var recordSet = score > highScore
            if (recordSet) {
                localStorage['highScore_' + difficulty] = JSON.stringify(score)
            }

            hideBoard(function () {})
            board.disable()
            var donePanel = DonePanel(time, steps, score, highScore, function () {
                donePanel.disable()
                donePanel.hide(function () {})
                if (recordSet) menu.updateHighScore()
                showMenu()
            })
            element.appendChild(donePanel.element)
            setTimeout(function () {
                donePanel.show(donePanel.enable)
            }, 50)

        })
        element.appendChild(board.element)
        setTimeout(function () {
            board.show(board.enable)
        }, 50)

    }

    var sizes = {
        easy: {
            horizontal: 4,
            vertical: 5,
        },
        medium: {
            horizontal: 5,
            vertical: 6,
        },
        hard: {
            horizontal: 6,
            vertical: 7,
        },
    }

    var menu = Menu(function (difficulty) {
        menu.disable()
        menu.hide(function () {
            element.removeChild(menu.element)
        })
        newBoard(difficulty)
    })

    var element = Div('MainPanel')

    showMenu()

    return { element: element }

}
;
function Menu (startListener) {

    function updateHighScore () {
        highScoreNode.nodeValue = GetHighScore(difficulty)
    }

    var difficulty = 'easy'

    var easyButton = DifficultyButton('EASY', 'easy', function () {
        difficulty = 'easy'
        mediumButton.deselect()
        hardButton.deselect()
        updateHighScore()
    })
    easyButton.select()

    var mediumButton = DifficultyButton('MEDIUM', 'medium', function () {
        difficulty = 'medium'
        easyButton.deselect()
        hardButton.deselect()
        updateHighScore()
    })

    var hardButton = DifficultyButton('HARD', 'hard', function () {
        difficulty = 'hard'
        easyButton.deselect()
        mediumButton.deselect()
        updateHighScore()
    })

    var classPrefix = 'Menu'

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode('NUMBER PAIRS'))

    var difficultyElement = Div(classPrefix + '-difficulty')
    difficultyElement.appendChild(TextNode('DIFFICULTY:'))

    var highScoreNode = TextNode('0')

    var highScoreElement = Div(classPrefix + '-highScore')
    highScoreElement.appendChild(TextNode('HIGH SCORE: '))
    highScoreElement.appendChild(highScoreNode)

    var startButton = Div(classPrefix + '-startButton Button')
    startButton.appendChild(TextNode('START'))

    var startClick = OnClick(startButton, function () {
        startListener(difficulty)
    })

    var contentElement = Div(classPrefix + '-content Scale')
    contentElement.appendChild(titleElement)
    contentElement.appendChild(difficultyElement)
    contentElement.appendChild(easyButton.element)
    contentElement.appendChild(mediumButton.element)
    contentElement.appendChild(hardButton.element)
    contentElement.appendChild(highScoreElement)
    contentElement.appendChild(startButton)

    var contentClassList = contentElement.classList

    var element = Div(classPrefix + ' BackgroundFade')
    element.appendChild(contentElement)

    var classList = element.classList

    updateHighScore()

    return {
        element: element,
        updateHighScore: updateHighScore,
        disable: function () {
            easyButton.disable()
            mediumButton.disable()
            hardButton.disable()
            startClick.disable()
        },
        enable: function () {
            easyButton.enable()
            mediumButton.enable()
            hardButton.enable()
            startClick.enable()
        },
        hide: function (callback) {
            classList.remove('visible')
            contentClassList.remove('visible')
            setTimeout(callback, 150)
        },
        show: function (callback) {
            classList.add('visible')
            contentClassList.add('visible')
            setTimeout(callback, 150)
        },
    }

}
;
function OnClick (element, listener) {

    function click () {
        classList.add('active')
        clearTimeout(timeout)
        timeout = setTimeout(function () {
            listener()
            timeout = setTimeout(function () {
                classList.remove('active')
            }, 150)
        }, 100)
    }

    function mouseDown (e) {
        if (e.button !== 0) return
        e.preventDefault()
        if (touched) touched = false
        else click()
    }

    function touchStart (e) {
        e.preventDefault()
        touched = true
        click()
    }

    var timeout

    var classList = element.classList

    var touched = false

    return {
        disable: function () {
            element.removeEventListener('touchstart', touchStart)
            element.removeEventListener('mousedown', mouseDown)
        },
        enable: function () {
            element.addEventListener('mousedown', mouseDown)
            element.addEventListener('touchstart', touchStart)
        },
    }

}
;
function PausePanel (resumeListener, restartListener, quitListener) {

    var classPrefix = 'PausePanel'

    var resumeElement = Div(classPrefix + '-resume Button')
    resumeElement.appendChild(TextNode('RESUME'))

    var resumeClick = OnClick(resumeElement, resumeListener)

    var restartElement = Div(classPrefix + '-restart Button')
    restartElement.appendChild(TextNode('RESTART'))

    var restartClick = OnClick(restartElement, restartListener)

    var quitElement = Div(classPrefix + '-quit Button')
    quitElement.appendChild(TextNode('QUIT'))

    var quitClick = OnClick(quitElement, quitListener)

    var contentElement = Div(classPrefix + '-content Scale')
    contentElement.appendChild(resumeElement)
    contentElement.appendChild(restartElement)
    contentElement.appendChild(quitElement)

    var contentClassList = contentElement.classList

    var element = Div(classPrefix + ' BackgroundFade')
    element.appendChild(contentElement)

    var classList = element.classList

    return {
        element: element,
        disable: function () {
            resumeClick.disable()
            restartClick.disable()
            quitClick.disable()
        },
        enable: function () {
            resumeClick.enable()
            restartClick.enable()
            quitClick.enable()
        },
        hide: function (callback) {
            classList.remove('visible')
            contentClassList.remove('visible')
            setTimeout(callback, 150)
        },
        show: function (callback) {
            classList.add('visible')
            contentClassList.add('visible')
            setTimeout(callback, 150)
        },
    }

}
;
function Shuffle (items) {
    for (var i = 0; i < items.length; i++) {
        var item = items[i]
        var randomIndex = i + Math.floor(Math.random() * (items.length - i))
        items[i] = items[randomIndex]
        items[randomIndex] = item
    }
    return items
}
;
function Steps () {

    var value = 0,
        maxValue = 1000

    var node = TextNode('0')

    var element = Div('Steps')
    element.appendChild(node)
    element.style.backgroundImage = 'url(images/steps.svg)'

    return {
        element: element,
        getValue: function () {
            return value
        },
        add: function () {
            value++
            node.nodeValue = value > maxValue ? '>' + maxValue : value
        },
    }

}
;
function TextNode (text) {
    return document.createTextNode(text)
}
;
function Time () {

    function beginInterval (tick) {
        interval = setInterval(tick, 100)
    }

    function largeTick () {
        value++
    }

    function smallTick () {
        value++
        if (value > maxValue) {
            clearInterval(interval)
            node.nodeValue = '>' + FormatTime(maxValue)
            interval = beginInterval(largeTick)
        } else {
            node.nodeValue = FormatTime(value)
        }
    }

    var value = 0,
        maxValue = 10000

    var node = TextNode('0')

    var element = Div('Time')
    element.appendChild(node)
    element.style.backgroundImage = 'url(images/time.svg)'

    var interval

    return {
        element: element,
        getValue: function () {
            return value
        },
        pause: function () {
            clearInterval(interval)
        },
        resume: function () {
            if (value > maxValue) beginInterval(largeTick)
            else beginInterval(smallTick)
        },
    }

}
;
(function () {

    ;(function () {
        var style = document.createElement('style')
        style.innerHTML =
            '@font-face {' +
                'font-family: FreeMono;' +
                'src: url(fonts/FreeMonoBold.ttf);' +
                'src: local("FreeMono Bold"), url(fonts/FreeMonoBold.ttf);' +
                'font-weight: bold;' +
            '}'
        document.head.appendChild(style)
    })()

    var loadPanel = LoadPanel(function () {
        var mainPanel = MainPanel()
        body.insertBefore(mainPanel.element, loadPanel.element)
        setTimeout(function () {
            loadPanel.hide(function () {
                body.removeChild(loadPanel.element)
            })
        }, 150)
    })

    var body = document.body
    body.appendChild(loadPanel.element)

})()
;

})()