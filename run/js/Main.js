(function () {

    ;(function () {
        var style = document.createElement('style')
        style.innerHTML =
            '@font-face {' +
                'font-family: FreeMono;' +
                'src: url(fonts/FreeMonoBold.ttf);' +
                'src: local("FreeMono Bold"), url(fonts/FreeMonoBold.ttf);' +
                'font-weight: bold;' +
            '}'
        document.head.appendChild(style)
    })()

    var loadPanel = LoadPanel(function () {
        var mainPanel = MainPanel()
        body.insertBefore(mainPanel.element, loadPanel.element)
        setTimeout(function () {
            loadPanel.hide(function () {
                body.removeChild(loadPanel.element)
            })
        }, 150)
    })

    var body = document.body
    body.appendChild(loadPanel.element)

})()
