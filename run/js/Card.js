function Card (number, x, y, clickListener) {

    var classPrefix = 'Card'

    var alignerElement = Div(classPrefix + '-aligner')

    var numberElement = Div(classPrefix + '-number')
    numberElement.appendChild(TextNode(number))

    var sideElement = Div(classPrefix + '-side')
    sideElement.appendChild(alignerElement)
    sideElement.appendChild(numberElement)

    var contentElement = Div(classPrefix + '-content Button')
    contentElement.appendChild(sideElement)

    var click = OnClick(contentElement, clickListener)

    var element = Div(classPrefix + ' x' + x + ' y' + y)
    element.appendChild(contentElement)

    var contentClassList = contentElement.classList

    var classList = element.classList

    var isOpen = false

    var timeout

    return {
        disable: click.disable,
        element: element,
        enable: click.enable,
        number: number,
        close: function () {
            if (isOpen) {
                isOpen = false
                contentClassList.remove('open')
                classList.add('animating')
                clearTimeout(timeout)
                timeout = setTimeout(function () {
                    contentClassList.remove('halfOpen')
                    contentClassList.remove('sideVisible')
                    timeout = setTimeout(function () {
                        classList.remove('animating')
                    }, 150)
                }, 150)
            }
        },
        hide: function (callback) {
            click.disable()
            setTimeout(function () {
                contentClassList.add('guessed')
                setTimeout(function () {
                    contentClassList.add('hidden')
                    setTimeout(callback, 350)
                }, 150)
            }, 150)
        },
        isOpen: function () {
            return isOpen
        },
        open: function () {
            if (!isOpen) {
                isOpen = true
                contentClassList.add('halfOpen')
                classList.add('animating')
                clearTimeout(timeout)
                timeout = setTimeout(function () {
                    contentClassList.add('open')
                    contentClassList.add('sideVisible')
                    timeout = setTimeout(function () {
                        classList.remove('animating')
                    }, 150)
                }, 150)
            }
        },
    }

}
