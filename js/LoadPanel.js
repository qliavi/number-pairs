function LoadPanel (loadCallback) {

    function loadNext () {
        var img = new Image
        img.src = 'images/' + images.shift() + '.svg'
        img.onload = function () {
            loaded++
            barElement.style.width = loaded / itemsToLoad * 100 + '%'
            if (loaded == itemsToLoad) loadCallback()
            else loadNext()
        }
    }

    var images = ['difficulty', 'selected', 'steps', 'time']

    var classPrefix = 'LoadPanel'

    var barElement = Div(classPrefix + '-bar')

    var element = Div(classPrefix)
    element.appendChild(barElement)

    var loaded = 0,
        itemsToLoad = images.length

    loadNext()

    return {
        element: element,
        hide: function (callback) {
            element.classList.add('hidden')
            setTimeout(callback, 350)
        },
    }

}
