<?php

function get_revisions () {
    return [
        'images/icons/16.png' => 4,
        'images/icons/32.png' => 4,
        'images/icons/64.png' => 2,
        'images/icons/90.png' => 4,
        'images/icons/120.png' => 4,
        'images/icons/128.png' => 4,
        'images/icons/256.png' => 4,
        'images/icons/512.png' => 1,
    ];
}
