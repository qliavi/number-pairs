<?php

include_once '../fns/get_run_revisions.php';
$revisions = get_run_revisions();

header('Content-Type: text/cache-manifest');

echo
    "CACHE MANIFEST\n"
    ."# v1\n"
    ."../fonts/FreeMonoBold.ttf\n"
    .'../compressed.css?'.$revisions['compressed.css']."\n"
    .'../compressed.js?'.$revisions['compressed.js']."\n"
    ."../images/difficulty.svg\n"
    ."../images/selected.svg\n"
    ."../images/steps.svg\n"
    ."../images/time.svg\n";
