#!/usr/bin/env node

process.chdir(__dirname)
process.chdir('..')

var fs = require('fs'),
    uglifyCss = require('uglifycss')

var files = [
    'Main',
    'BackgroundFade',
    'Scale',
    'Board',
    'Button',
    'Card',
    'DonePanel',
    'easyCard',
    'mediumCard',
    'hardCard',
    'DifficultyButton',
    'LoadPanel',
    'MainPanel',
    'PausePanel',
    'Steps',
    'Time',
    'Menu',
]

var source = ''
files.forEach(function (file) {
    source += fs.readFileSync('css/' + file + '.css', 'utf-8') + '\n'
})

var compressCss = uglifyCss.processString(source)
fs.writeFileSync('compressed.css', compressCss)
