#!/usr/bin/env node

process.chdir(__dirname)
process.chdir('..')

var fs = require('fs')
var uglifyJs = require('uglify-js')

var files = [
    'Div',
    'Board',
    'Card',
    'DifficultyButton',
    'DonePanel',
    'FormatTime',
    'GetHighScore',
    'LoadPanel',
    'MainPanel',
    'Menu',
    'OnClick',
    'PausePanel',
    'Shuffle',
    'Steps',
    'TextNode',
    'Time',
    'Main',
]

var source = '(function () {\n'
files.forEach(function (file) {
    source += fs.readFileSync('js/' + file + '.js', 'utf8') + ';\n'
})
source += '\n})()'

var compressedSource = uglifyJs.minify({ 'combined.js': source }).code

fs.writeFileSync('combined.js', source)
fs.writeFileSync('compressed.js', compressedSource)
