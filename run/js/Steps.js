function Steps () {

    var value = 0,
        maxValue = 1000

    var node = TextNode('0')

    var element = Div('Steps')
    element.appendChild(node)
    element.style.backgroundImage = 'url(images/steps.svg)'

    return {
        element: element,
        getValue: function () {
            return value
        },
        add: function () {
            value++
            node.nodeValue = value > maxValue ? '>' + maxValue : value
        },
    }

}
