function Menu (startListener) {

    function updateHighScore () {
        highScoreNode.nodeValue = GetHighScore(difficulty)
    }

    var difficulty = 'easy'

    var easyButton = DifficultyButton('EASY', 'easy', function () {
        difficulty = 'easy'
        mediumButton.deselect()
        hardButton.deselect()
        updateHighScore()
    })
    easyButton.select()

    var mediumButton = DifficultyButton('MEDIUM', 'medium', function () {
        difficulty = 'medium'
        easyButton.deselect()
        hardButton.deselect()
        updateHighScore()
    })

    var hardButton = DifficultyButton('HARD', 'hard', function () {
        difficulty = 'hard'
        easyButton.deselect()
        mediumButton.deselect()
        updateHighScore()
    })

    var classPrefix = 'Menu'

    var titleElement = Div(classPrefix + '-title')
    titleElement.appendChild(TextNode('NUMBER PAIRS'))

    var difficultyElement = Div(classPrefix + '-difficulty')
    difficultyElement.appendChild(TextNode('DIFFICULTY:'))

    var highScoreNode = TextNode('0')

    var highScoreElement = Div(classPrefix + '-highScore')
    highScoreElement.appendChild(TextNode('HIGH SCORE: '))
    highScoreElement.appendChild(highScoreNode)

    var startButton = Div(classPrefix + '-startButton Button')
    startButton.appendChild(TextNode('START'))

    var startClick = OnClick(startButton, function () {
        startListener(difficulty)
    })

    var contentElement = Div(classPrefix + '-content Scale')
    contentElement.appendChild(titleElement)
    contentElement.appendChild(difficultyElement)
    contentElement.appendChild(easyButton.element)
    contentElement.appendChild(mediumButton.element)
    contentElement.appendChild(hardButton.element)
    contentElement.appendChild(highScoreElement)
    contentElement.appendChild(startButton)

    var contentClassList = contentElement.classList

    var element = Div(classPrefix + ' BackgroundFade')
    element.appendChild(contentElement)

    var classList = element.classList

    updateHighScore()

    return {
        element: element,
        updateHighScore: updateHighScore,
        disable: function () {
            easyButton.disable()
            mediumButton.disable()
            hardButton.disable()
            startClick.disable()
        },
        enable: function () {
            easyButton.enable()
            mediumButton.enable()
            hardButton.enable()
            startClick.enable()
        },
        hide: function (callback) {
            classList.remove('visible')
            contentClassList.remove('visible')
            setTimeout(callback, 150)
        },
        show: function (callback) {
            classList.add('visible')
            contentClassList.add('visible')
            setTimeout(callback, 150)
        },
    }

}
