function DifficultyButton (text, className, clickListener) {

    function select () {
        classList.add('selected')
    }

    var iconElement = Div('DifficultyButton-icon ' + className)
    iconElement.style.backgroundImage = 'url(images/difficulty.svg)'

    var selectedElement = Div('DifficultyButton-selected')
    selectedElement.style.backgroundImage = 'url(images/selected.svg)'

    var element = Div('DifficultyButton Button ' + className)
    element.appendChild(iconElement)
    element.appendChild(TextNode(text))
    element.appendChild(selectedElement)

    var click = OnClick(element, function () {
        select()
        clickListener()
    })

    var classList = element.classList

    return {
        disable: click.disable,
        element: element,
        enable: click.enable,
        select: select,
        deselect: function () {
            classList.remove('selected')
        },
    }

}
